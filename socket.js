const net = require('net');
const server = net.createServer();
const TCP_PORT = 6974
const queries = require('./server/queries')
const connection = require('./server/database')

//emitted when server closes ...not emitted until all connections closes.
server.on('close',function(){
	console.log('[TCP-] TCP Comm Server closed! What?');
});

// emitted when new client connects
server.on('connection',function(socket){
	socket.pipe(socket)
    var rport = socket.remotePort;
    var raddr = socket.remoteAddress;
    console.log('[TCP+] New client connection established:' + rport);
    //var no_of_connections =  server.getConnections(); // sychronous version
    server.getConnections(function(error,count){
        console.log('Number of concurrent connections to the server : ' + count);
    });

	socket.pipe(socket)
	socket.setEncoding('utf8');
	socket.setKeepAlive(true, 5000)
    
    socket.on('drain',function(){
        console.log('[TCP-] TCP Drain. Write buffer is empty.');
        socket.resume();
    });

    socket.on('error',function(error){
        console.log('[TCP-] Error : ' + error);
    });

    socket.on('timeout',function(){
        console.log('[TCP-] Socket timed out !');
        socket.end('Timed out!');
    // can call socket.destroy() here too.
    });
    
    socket.on('data',function(data){
        doProcess(socket, data)
    });
});

// emits when any error occurs -> calls closed event immediately after this.
server.on('error',function(error){
  	console.log('[TCP-] Error: ' + error);
});

//emits when server is bound with server.listen
server.on('listening',function(){
 	 console.log('[TCP+] Server is listening!');
});

server.maxConnections = 3;

// for dyanmic port allocation
server.listen(TCP_PORT, function(){
	var address = server.address();
	var port = address.port;
	var family = address.family;
	var ipaddr = address.address;
	console.log('[TCP+] Server is listening at port' + port);
	console.log('[TCP+] Server ip :' + ipaddr);
	console.log('[TCP+] Server is IP4/IP6 : ' + family);
});

// for log purpose only!
const powerTypes = {
    [0]: "FLYING SHOES",
    [1]: "BOMB POWER",
    [2]: "MORE BOMBS",
    [3]: "SPEED SHOES",
}
const queryTypes = {
    [0]: queries.addFly,
    [1]: queries.addPower,
    [2]: queries.addBomb,
    [3]: queries.addFly,
}

const playerACtivities = {
    // t: Type, p: Player, w: What
    [0]: ({t, p, w}) => {
        // pickup power
        console.log(`PLAYER(${p}) picked up "${powerTypes[w]}".`)
        connection.query(queryTypes[w], [p], (error, result, fields) => {
            if (error) {
                console.warn(error)
            }
        })
        connection.query(queries.addMatchPowerups, [p], (error) => {
            if (error) {
                console.warn(error)
            }
        })
    },
    [1]: ({t, p}) => {
        // killed someone
        console.log(`PLAYER(${p}) scored a kill!`)
        connection.query(queries.addKill, [p], (error) => {
            if (error) {
                console.warn(error)
            }
        })
    },
    [2]: ({t, p}) => {
        // died by someone
        console.log(`PLAYER(${p}) has died.`)
    },
    [3]: ({t, p}) => {
        // placed a bomb
        console.log("placed a bomb")
        console.log(`PLAYER(${p}) just placed a bomb.`)
        connection.query(queries.placeBomb, [p], (error, result, fields) => {
            if (error) {
                console.warn(error)
            }
        })
        connection.query(queries.addMatchBombs, [p], (error) => {
            if (error) {
                console.warn(error)
            }
        })
    },
    [4]: ({t, p}) => {
        // joined the game and create the record.
        console.log("create the records")
        console.log(`PLAYER(${p}) has joined the game.`)
        connection.query(queries.startMatch, [p], (error) => {
            if (error) {
                console.warn(error)
            }
        })
    }, 
    [5]: ({t, p}) => {
        // find the latest end time null record and fill it.
        // QUERY: SELECT * FROM match WHERE playerid == ? ORDER BY match.start DESC LIMIT 1 <<
        /*
            SELECT * FROM matches_list
                WHERE matches_list.playerID = 1 AND matches_list.quitTime IS NULL
                ORDER BY matches_list.joinTime DESC
                LIMIT 1
        */
        console.log("end latest match data")
        console.log(`PLAYER(${p}) left the game.`)
        console.log(`attempting to close latest match record on PLAYER(${p})`)
        connection.query(queries.endMatch, [p], (error) => {
            if (error) {
                console.warn(error)
            }
        })
    }
}

function doProcess(socket, data) {
	if (data[0] && data[0] == '*') {
		//console.log("[GAME] RECEIVED PLAYER LOG")
		try {
			data = data.substring(1)
			data = JSON.parse(data)
            
            const dataFunc = data.t != undefined && playerACtivities[data.t] 
            
            if (dataFunc) {
                dataFunc(data)
            } else {
                console.warn(`[TCP] WARNING! ACTIVITY TYPE ${data.t} IS NOT DEFINED!`)
            }
		} catch (error) {
			console.warn("[TCP] TCP DATA PARSING FAILED!")
			console.warn(error)			
		}
	}
}

module.exports = server