-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.3.11-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win32
-- HeidiSQL 버전:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- handshake 데이터베이스 구조 내보내기
DROP DATABASE IF EXISTS `handshake`;
CREATE DATABASE IF NOT EXISTS `handshake` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `handshake`;

-- 테이블 handshake.forum_comments 구조 내보내기
DROP TABLE IF EXISTS `forum_comments`;
CREATE TABLE IF NOT EXISTS `forum_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(32) NOT NULL,
  `post_id` int(32) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_comments_fk0` (`player_id`),
  KEY `forum_comments_fk1` (`post_id`),
  CONSTRAINT `forum_comments_fk0` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`),
  CONSTRAINT `forum_comments_fk1` FOREIGN KEY (`post_id`) REFERENCES `forum_posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- 테이블 데이터 handshake.forum_comments:~5 rows (대략적) 내보내기
DELETE FROM `forum_comments`;
/*!40000 ALTER TABLE `forum_comments` DISABLE KEYS */;
INSERT INTO `forum_comments` (`id`, `player_id`, `post_id`, `content`) VALUES
	(1, 3, 1, '전 잘 모르겠던데 ㅎㅎ'),
	(2, 5, 2, '아 머하는거야'),
	(3, 4, 4, '뻘글 쓰지 마라'),
	(4, 2, 1, '패스파인더 왜씀? 지브롤터 쓰면 되느데 ㅋㅋㅋㅋㅋㅋ'),
	(5, 4, 1, '어떤놈이 징징거리는가? 대체 어떤놈이 불평을 하는건가!');
/*!40000 ALTER TABLE `forum_comments` ENABLE KEYS */;

-- 테이블 handshake.forum_likes 구조 내보내기
DROP TABLE IF EXISTS `forum_likes`;
CREATE TABLE IF NOT EXISTS `forum_likes` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `player_id` int(32) NOT NULL,
  `post_id` int(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_likes_fk0` (`player_id`),
  KEY `forum_likes_fk1` (`post_id`),
  CONSTRAINT `forum_likes_fk0` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`),
  CONSTRAINT `forum_likes_fk1` FOREIGN KEY (`post_id`) REFERENCES `forum_posts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 테이블 데이터 handshake.forum_likes:~2 rows (대략적) 내보내기
DELETE FROM `forum_likes`;
/*!40000 ALTER TABLE `forum_likes` DISABLE KEYS */;
INSERT INTO `forum_likes` (`id`, `player_id`, `post_id`) VALUES
	(1, 1, 1),
	(2, 4, 1);
/*!40000 ALTER TABLE `forum_likes` ENABLE KEYS */;

-- 테이블 handshake.forum_posts 구조 내보내기
DROP TABLE IF EXISTS `forum_posts`;
CREATE TABLE IF NOT EXISTS `forum_posts` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `player_id` int(32) NOT NULL,
  `title` mediumtext NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_posts_fk0` (`player_id`),
  CONSTRAINT `forum_posts_fk0` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- 테이블 데이터 handshake.forum_posts:~5 rows (대략적) 내보내기
DELETE FROM `forum_posts`;
/*!40000 ALTER TABLE `forum_posts` DISABLE KEYS */;
INSERT INTO `forum_posts` (`id`, `player_id`, `title`, `content`) VALUES
	(1, 1, '패스파인더 버프좀 해줘', '아니 이 캐릭터가 할 수 있는게 없어서 이 캐릭터로 게임할때마다 게임 할맛이 뚝뚝 떨어짐. 어케점 해봐요'),
	(2, 4, '서버 이상해요', '서버가 자꾸 죽었다고 떠서 게임 판만 4개 날려먹었는데 이거 복구 되는건가요? 계속 이러니까 게임 할맛 안나는거 같아요'),
	(3, 3, '테스트 글', '테스트 글입니다'),
	(4, 1, '뻘글 갸꼴', '뻘글 쓰면 경험치 주네'),
	(5, 1, '에이펙스', '킬 500임');
/*!40000 ALTER TABLE `forum_posts` ENABLE KEYS */;

-- 테이블 handshake.game_clan 구조 내보내기
DROP TABLE IF EXISTS `game_clan`;
CREATE TABLE IF NOT EXISTS `game_clan` (
  `id` varchar(16) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `short` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 handshake.game_clan:~0 rows (대략적) 내보내기
DELETE FROM `game_clan`;
/*!40000 ALTER TABLE `game_clan` DISABLE KEYS */;
INSERT INTO `game_clan` (`id`, `name`, `short`) VALUES
	('DVEPM2t1hpTNoly5', 'I am Groot', 'IMGT');
/*!40000 ALTER TABLE `game_clan` ENABLE KEYS */;

-- 테이블 handshake.game_leader 구조 내보내기
DROP TABLE IF EXISTS `game_leader`;
CREATE TABLE IF NOT EXISTS `game_leader` (
  `id` varchar(16) NOT NULL COMMENT '유저 식별 번호',
  `score` int(11) DEFAULT NULL COMMENT '최종 스코어',
  `woc` varchar(32) DEFAULT NULL COMMENT '사용 무기',
  `kill` int(11) DEFAULT NULL COMMENT '킬수',
  `objective` tinyint(4) DEFAULT NULL COMMENT '임무 완수 개수',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 MAX_ROWS=50;

-- 테이블 데이터 handshake.game_leader:~13 rows (대략적) 내보내기
DELETE FROM `game_leader`;
/*!40000 ALTER TABLE `game_leader` DISABLE KEYS */;
INSERT INTO `game_leader` (`id`, `score`, `woc`, `kill`, `objective`) VALUES
	('8a4lie1RuO8j8MQE', 74113, 'shotgun', 4537, 2),
	('9RgJkGjrvDJcpn5Z', 208020, 'knife', 4175, 1),
	('B9vnUcKPgGMYZUgz', 893684, 'ar2', 3989, 1),
	('bUzTqdpmUFcuon0B', 965455, 'smg1', 3007, 3),
	('FABZYKdmONOVMrER', 74338, 'pistol', 4598, 3),
	('fS9u54tNOsoMtkZx', 103516, 'smg1', 4036, 1),
	('gyH5a30y4K4KEjSc', 186411, 'smg1', 79, 2),
	('j54scYtCi4ekGL3Q', 34781, 'smg1', 1878, 1),
	('jtjkEeWABOwAnSpP', 942307, 'grenade', 3115, 2),
	('Li5Rkt6WXp2MK0DR', 368223, 'smg1', 4327, 2),
	('mjTaZ2Bvi11yyKFI', 62697, 'pistol', 4936, 3),
	('PXzwpLjtpTB1rNf8', 319412, 'smg1', 4259, 1),
	('wsJIgNAyiiHit32x', 328942, 'grenade', 3857, 3);
/*!40000 ALTER TABLE `game_leader` ENABLE KEYS */;

-- 테이블 handshake.leaderboard 구조 내보내기
DROP TABLE IF EXISTS `leaderboard`;
CREATE TABLE IF NOT EXISTS `leaderboard` (
  `id_player` int(32) DEFAULT NULL,
  KEY `playerid` (`id_player`),
  CONSTRAINT `FK_leaderboard_player` FOREIGN KEY (`id_player`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='comparison table\r\n';

-- 테이블 데이터 handshake.leaderboard:~5 rows (대략적) 내보내기
DELETE FROM `leaderboard`;
/*!40000 ALTER TABLE `leaderboard` DISABLE KEYS */;
INSERT INTO `leaderboard` (`id_player`) VALUES
	(1),
	(2),
	(3),
	(4),
	(5);
/*!40000 ALTER TABLE `leaderboard` ENABLE KEYS */;

-- 테이블 handshake.matches_list 구조 내보내기
DROP TABLE IF EXISTS `matches_list`;
CREATE TABLE IF NOT EXISTS `matches_list` (
  `playerID` int(11) NOT NULL,
  `joinTime` datetime NOT NULL,
  `quitTime` datetime DEFAULT NULL,
  `kills` int(32) NOT NULL DEFAULT 0,
  `powerups` int(32) NOT NULL DEFAULT 0,
  `bombs` int(32) NOT NULL DEFAULT 0,
  `score` int(32) NOT NULL DEFAULT 0,
  KEY `playerID` (`playerID`),
  CONSTRAINT `playerID` FOREIGN KEY (`playerID`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 handshake.matches_list:~10 rows (대략적) 내보내기
DELETE FROM `matches_list`;
/*!40000 ALTER TABLE `matches_list` DISABLE KEYS */;
INSERT INTO `matches_list` (`playerID`, `joinTime`, `quitTime`, `kills`, `powerups`, `bombs`, `score`) VALUES
	(1, '2019-04-10 21:53:40', '2019-05-14 19:43:02', 5, 3, 55, 0),
	(1, '2019-04-10 21:24:17', '2019-04-10 21:24:40', 5, 3, 55, 0),
	(1, '2019-04-10 21:24:40', '2019-04-10 21:53:30', 5, 3, 55, 0),
	(148, '2019-05-14 19:50:35', '2019-05-14 19:52:17', 0, 0, 0, 0),
	(148, '2019-05-14 19:52:16', '2019-05-14 19:52:17', 0, 0, 0, 0),
	(148, '2019-05-14 19:52:32', '2019-05-14 19:53:16', 0, 0, 0, 0),
	(148, '2019-05-14 19:54:47', '2019-05-14 20:01:24', 0, 8, 5, 0),
	(148, '2019-05-14 20:01:26', '2019-05-14 20:02:03', 0, 0, 0, 0),
	(148, '2019-05-14 20:04:05', '2019-05-14 20:34:39', 0, 9, 3, 0),
	(148, '2019-05-14 20:35:41', '2019-05-14 21:24:36', 0, 0, 0, 0);
/*!40000 ALTER TABLE `matches_list` ENABLE KEYS */;

-- 테이블 handshake.player 구조 내보내기
DROP TABLE IF EXISTS `player`;
CREATE TABLE IF NOT EXISTS `player` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `kills` int(32) NOT NULL DEFAULT 0,
  `objectives` int(32) NOT NULL DEFAULT 0,
  `score` int(32) NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '이름 없음',
  `tags` varchar(512) NOT NULL DEFAULT '{}',
  `item_speed` int(32) NOT NULL DEFAULT 0,
  `item_power` int(32) NOT NULL DEFAULT 0,
  `item_bomb` int(32) NOT NULL DEFAULT 0,
  `item_fly` int(32) NOT NULL DEFAULT 0,
  `bomb_placed` int(32) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

-- 테이블 데이터 handshake.player:~6 rows (대략적) 내보내기
DELETE FROM `player`;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
INSERT INTO `player` (`id`, `kills`, `objectives`, `score`, `name`, `tags`, `item_speed`, `item_power`, `item_bomb`, `item_fly`, `bomb_placed`) VALUES
	(1, 238, 231, 0, '원챔충', '{}', 0, 0, 0, 0, 0),
	(2, 132, 123, 123, '불꽃싸대기', '{}', 0, 0, 0, 0, 0),
	(3, 1, 1, 1, '늅늅이', '{}', 0, 0, 0, 0, 0),
	(4, 3, 3, 3, 'bABO3995', '{}', 0, 0, 0, 0, 0),
	(5, 140, 120, 124490, 'John Doe', '{}', 0, 20, 6, 4, 61),
	(148, 0, 0, 250, 'rebel1324', '{}', 0, 21, 9, 14, 25);
/*!40000 ALTER TABLE `player` ENABLE KEYS */;

-- 테이블 handshake.service_user 구조 내보내기
DROP TABLE IF EXISTS `service_user`;
CREATE TABLE IF NOT EXISTS `service_user` (
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_id` int(32) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `user_fk0` (`user_id`),
  CONSTRAINT `user_fk0` FOREIGN KEY (`user_id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='user와 player를 나눈 이유\r\n만약 플레이어가 탈퇴를 해도, 탈퇴한 플레이어에 대한 릴레이션이 남아있어야 한다. \r\n계정 삭제는 단지 이 player객체를 사용할 권한을 잃어 버리는 것.';

-- 테이블 데이터 handshake.service_user:~0 rows (대략적) 내보내기
DELETE FROM `service_user`;
/*!40000 ALTER TABLE `service_user` DISABLE KEYS */;
INSERT INTO `service_user` (`email`, `password`, `user_id`) VALUES
	('rebel1324@gmail.com', 'fpzk1324', 148);
/*!40000 ALTER TABLE `service_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
