export const comma = (str) => {
    try {
        return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } catch (error) {
        console.log(error)
        return "[ERROR]"
    }
}
