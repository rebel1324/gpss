import axios from 'axios'
import endpoints from '../../server/endpoints'

const getInfo = async (token) => {
    const request = await axios.post(endpoints.ping, {
        token: token
    })

    return request ? request.data : null
}
export default getInfo