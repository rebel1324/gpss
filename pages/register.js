import Hero from './components/hero';
import Page from './components/layout';
import _ from 'lodash'
import {
  Container,
  Divider,
  Icon,
  Header,
  Table,
  Segment,
  Card,
  Input,
  Form,
  Button,
  Message,
} from 'semantic-ui-react'

import {comma} from './utils/string';
import endpoints from '../server/endpoints'
import axios from 'axios'
  
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const validChecks = [
    (me) => { if (!emailRegex.exec(me.state.email)) { return {error: {email: true}, errorMessage: 'Invalid e-mail address.'} } },
    
    (me) => { if (!me.state.password) { return {error: {password: true}, errorMessage: 'Password is empty.'} } },
    (me) => { if (me.state.password.length > 33) { return {error: {password: true}, errorMessage: 'Password is too long. It must be shorter than 32 characters.'} } },
    (me) => { if (me.state.password.length < 7) { return {error: {password: true}, errorMessage: 'Password is too short. It muse be longer than 8 characters.'} } },
    (me) => { if (!me.state.passwordConfirm) { return {error: {password: true}, errorMessage: 'Password Confirm is empty.'} } },
    (me) => { if (me.state.password != me.state.passwordConfirm) { return {error: {password: true}, errorMessage: 'Password and Confirm Password does not match.'} } },
    
    (me) => { if (!me.state.name) { return {error: {name: true}, errorMessage: 'Username is empty.'} } },
    (me) => { if (me.state.name.length < 3) { return {error: {name: true}, errorMessage: 'Username too short. It must be longer than 2 characters.'} } },
    (me) => { if (me.state.name.length > 12) { return {error: {name: true}, errorMessage: 'Username too long. It muse be shorter than 13 characters.'} } },
]

const failureTranslate = {
    name: (value) => `Username '${value}' is already taken.`,
    PRIMARY: (value) => `E-mail '${value}' is already registered to this service.`,
    user_id: (value) => `Database failure detected - Please contact developers with errorcode: DB.`
}


class Index extends React.Component {
    state = {
        email: "",
        password: "",
        passwordConfirm: "",
        name: "",
        tos: false,

        error: {
            email: false,
            password: false,
            name: false,
            tos: false
        },
        errorMessage: ''
    }

    handleChange = (event, {name, value, checked}) => this.setState( { 
        [name] : value,
        error: {
            email: false,
            password: false,
            name: false,
            tos: false
        },
        errorMessage: ''
    })

    handleSubmit = () => {
        const {errorMessage, email, password, passwordConfirm, name, tos} = this.state;
        
        try {
            for (let index = 0; index < validChecks.length; index++) {
                const errorInfo = validChecks[index](this)
                if (errorInfo) {
                    this.setState(errorInfo)
                    return false
                }
            }

            axios.post(endpoints.register, {
                email: email,
                password: password,
                name: name
            }).then(({data}) => {
                const {failed, failedKey, failedValue, failureCode} = data

                if (failed) {
                    if (failureTranslate[failedKey]) {
                        this.setState({
                            errorMessage: failureTranslate[failedKey](failedValue)
                        })
                    }
                } else {
                    // TODO: Redirect to welcome service or profile picture.
                    window.location.href = '/done'
                    console.log("what in the fuck")
                }
            }).catch(error => {
                console.warn(error)
            })
        } catch (error) {
            console.warn(error)
            this.setState({
                errorMessage: 'Sorry, Something went wrong.'
            })
        }
    }

    render = () => {
        const {errorMessage, email, password, passwordConfirm, name, tos} = this.state;

        return (
            <Page>
                <Hero>
                    <Header as='h2'>
                        <Icon name='user'/>
                        <Header.Content>
                            Register
                        <Header.Subheader>Register the account to this service.</Header.Subheader>
                        </Header.Content>
                    </Header>
                </Hero>
                <Container>
                    { 
                        (errorMessage != '') ? (<Message
                            header='Sorry, An error occured.'
                            error
                            content={errorMessage}
                        ></Message>) : undefined
                    }
                    
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Field required>
                            <Form.Input
                                label='E-mail'
                                type='email'
                                name='email'
                                value={email}
                                onChange={this.handleChange}
                                error={this.state.error.email}
                            ></Form.Input>
                        </Form.Field>
                        <Form.Field required>
                            <Form.Input
                                label='Password'
                                type='password'
                                name='password'
                                value={password}
                                onChange={this.handleChange}
                                error={this.state.error.password}
                            ></Form.Input>
                        </Form.Field>
                        <Form.Field required>
                            <Form.Input
                                label='Password Confrim'
                                type='password'
                                name='passwordConfirm'
                                value={passwordConfirm}
                                onChange={this.handleChange}
                                error={this.state.error.password}
                            ></Form.Input>
                        </Form.Field>
                        <Form.Field required>
                            <Form.Input
                                label='Username'
                                name='name'
                                value={name}
                                onChange={this.handleChange}
                                error={this.state.error.name}
                            ></Form.Input>
                        </Form.Field>
                        <Button primary icon='user' content='Submit' />
                    </Form>
                </Container> 
            </Page>
        );  
    }
}
export default Index;