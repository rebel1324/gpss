import { Grid, Divider, Header, Segment, Container, Button, Card, Image, Icon } from 'semantic-ui-react'
import Page from './components/layout'
import Hero from './components/hero'

const src = 'https://react.semantic-ui.com/images/wireframe/square-image.png'

const RepoHero = () => {
    return (<Hero className="hero-background">
        <Image src={src} size='small' centered circular />
        <Header as='h1' inverted>Project Handshake</Header>
        <Button inverted basic>
          <Icon name='download'/>
          Download Game
        </Button>
    </Hero>)
}
// This is the perfect and practical way to do next
// This will be the most used shit
const Introduction = () => {
	return (<div>
        <Hero>
            <div className='block-margin'></div>
			<Container textAlign='left'>
			<Header
				as='h2'
				content='Multiplayer Service'
				subheader='Good way to Start Unreal Engine 4 Multiplayer Service.'
			/>

			<p>Today's Gaming Industry is all about Multiplayer. Even Sigleplayer Games needs Internet Connection for better expereinces.</p>
			<p>I know that not all of people are good at making multiplayer game. Even don't get where to start.</p>
			<p>When you don't know where to go, This is one good choice to start up your own game with some web service.</p>
			</Container>
        </Hero>
		<Hero>
			<Container textAlign='left'>
			<Header
				as='h2'
				content='Open Source'
				subheader='All Source Code is public with MIT License.'
				/>

			<p>All of these works are released in public. You can fork and branch to your own game with GitHub</p>
			<p>Also with MIT License, You don't have to worry about license and stuffs. All you need is code.</p>
			<p>If you're pleased with the contents, feel free to leave stars and watch to our GitHub Repository.</p>
			</Container>
		</Hero>

	</div>)
}

const MediaHero = () => {
	return (<Hero>
        <Container>
            <Header
            as='h2'
            content='Media'
            subheader='This is what you get'
            />

            <Segment vertical>
                <Image.Group size='small'>
                <Image src={src} />
                <Image src={src} />
                <Image src={src} />
                </Image.Group>
            </Segment>
        </Container>
	</Hero>)
}


/*
	Team Hero

	This component generate the team card.
*/
const TeamCard = ({pic, name, role, desc, github, twitter}) => {
    let gitDOM = (<div></div>)
    let twitterDOM = (<div></div>)

    if (twitter) {
      twitterDOM = (<a href={twitter}><Icon size='large' name='twitter' /> </a>)
    }
    if (github) {
      gitDOM = (<a href={github}><Icon size='large' name='github' /></a>)
    }

    return (
      <Card>
        <Image src={pic} />
        <Card.Content>
          <Card.Header>{name}</Card.Header>
          <Card.Meta>
            <span className='date'>{role}</span>
          </Card.Meta>
          <Card.Description>
            <p>{desc}</p>
          </Card.Description>
        </Card.Content>
        
        <Card.Content extra>
          {gitDOM}
          {twitterDOM}
        </Card.Content>
      </Card>
    )
}

const teamList = [
	{
	  pic: '',
	  name: "Kyu Yeon \"rebel\" Lee",
	  role: "Project Leader / Server Programming",
	  desc: "The mememan who studied Computer Science and being bedroom programmer for 4 years. Tried to make some games but ended up making game framework and libaries.",
	  github: "http://github.com/rebel1324",
	  twitter: "https://twitter.com/_rebel1324",
	},
	{
	  pic: '',
	  name: "Sang Hwa Lee",
	  role: "QA / Client Progamming",
	  desc: "True Anime Boi who just want to make something good. Bretty good at making game clients. ",
	  github: "",
	  twitter: "https://twitter.com/sleuthl007",
	},
  ]
  
const TeamHero = () => {
    let genDOM = []
    teamList.forEach((data) => {
      genDOM.push((
        <TeamCard 
          pic={data.pic}
          name={data.name}
          quote={data.quote}
          role={data.role}
          desc={data.desc}
          history={data.history}
          key={data.name}
          github={data.github}
          twitter={data.twitter}
        />
      ))
	})
	
	return (<Hero>
        <Container>
			<Header
			as='h2'
			content='Contributors'
			subheader='We make this better by the time'
			/>
			<Card.Group>
			    {genDOM}
			</Card.Group>
		</Container>
	</Hero>)
}

const CommunicateHero = () => {
	return (<Hero>
        <Container>
          <Grid stackable>
            <Grid.Column floated='left' width={7}>
              <Header textAlign='left' as='h2' icon='question circle outline' content='Join Discussion' subheader='Share your ideas and thoughts'/>
            </Grid.Column>
            <Grid.Column floated='right' width={3}>
              <Button href='/leaderboard'>Join</Button>
            </Grid.Column>
          </Grid>

          <Divider horizontal>Or</Divider>

          <Grid stackable>
            <Grid.Column floated='left' width={7}>
              <Header textAlign='left' as='h2' icon='users' content='Leaderboard' subheader="Check who's winning the game!"/>
            </Grid.Column>
            <Grid.Column floated='right' width={3}>
              <Button href='/forum'>Join</Button>
            </Grid.Column>
          </Grid>
        </Container>
	</Hero>)
}

export default () => {
    return (<Page>
        <RepoHero></RepoHero>
		<Introduction></Introduction>
        <MediaHero></MediaHero>
		<TeamHero></TeamHero>
		<CommunicateHero></CommunicateHero>
    </Page>)
}