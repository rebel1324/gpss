import Hero from './components/hero';
import Page from './components/layout';
import _ from 'lodash'
import {
  Container,
  Divider,
  Icon,
  Header,
  Table,
  Segment,
  Card,
  Input,
  Form,
  Message,
  Button,
} from 'semantic-ui-react'

import {comma} from './utils/string';
import endpoints from '../server/endpoints'
import fetch from 'isomorphic-unfetch'
import axios from 'axios'
import { Cookies } from 'react-cookie'

const cookies = new Cookies();
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const validChecks = [
    (me) => { if (!emailRegex.exec(me.state.email)) { return {error: {email: true}, errorMessage: 'Invalid e-mail address.'} } },
    
    (me) => { if (!me.state.password) { return {error: {password: true}, errorMessage: 'Password is empty.'} } },
    (me) => { if (me.state.password.length > 33) { return {error: {password: true}, errorMessage: 'Password is too long. It must be shorter than 32 characters.'} } },
    (me) => { if (me.state.password.length < 7) { return {error: {password: true}, errorMessage: 'Password is too short. It muse be longer than 8 characters.'} } }
]

class Index extends React.Component {
    state = {
        email: "",
        password: "",
        passwordConfirm: "",
        name: "",
        tos: false,

        error: {
            email: false,
            password: false,
            name: false,
            tos: false
        },
        errorMessage: ''
    }

    handleSubmit = () => {        
        const {email, password} = this.state;

        try {
            for (let index = 0; index < validChecks.length; index++) {
                const errorInfo = validChecks[index](this)
                if (errorInfo) {
                    this.setState(errorInfo)
                    return false
                }
            }
            
            axios.post(endpoints.login, {
                email: email,
                password: password,
            }).then(({data}) => {
                const {token} = data

                if (token) {
                    cookies.set('token', token)
                    
                    this.setState({
                        token: token
                    })
                    
                    console.log("what the fuck")
                    window.location.href = '/profile'
                } else {
                    throw "WHATT"
                }
            }).catch(error => {
                throw error
            })
        } catch (error) {
            console.warn(error)
            this.setState({
                errorMessage: 'Sorry, Something went wrong.'
            })
        }
    }

    handleChange = (event, {name, value, checked}) => this.setState( { 
        [name] : value,
        error: {
            email: false,
            password: false,
            name: false,
            tos: false
        },
        errorMessage: ''
    })

    render() {
        const {errorMessage, email, password, passwordConfirm, name, tos} = this.state;
        
        return (
            <Page>
                <Card centered style={{marginTop: '6rem'}}>
                    <Card.Content>
                            { 
                                (errorMessage != '') ? (<Message
                                    header='Sorry, An error occured.'
                                    error
                                    content={errorMessage}
                                ></Message>) : (<p>
                                    Please login to die instantly
                                </p>)
                            }
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Field required>
                                <Form.Input
                                    label='E-mail'
                                    type='email'
                                    name='email'
                                    value={email}
                                    onChange={this.handleChange}
                                    error={this.state.error.email}
                                ></Form.Input>
                            </Form.Field>
                            <Form.Field required>
                                <Form.Input
                                    label='Password'
                                    type='password'
                                    name='password'
                                    value={password}
                                    onChange={this.handleChange}
                                    error={this.state.error.password}
                                ></Form.Input>
                            </Form.Field>
                            <Button primary icon='lock' content='Login' />
                            <p style={{marginTop: '10px'}}>
                                <div><a href="/register">Make a new account</a></div>
                                <div><a href="">I forgot my password!</a></div>
                            </p>
                        </Form>
                    </Card.Content>
                </Card>
            </Page>
        )
    }
}

export default Index;