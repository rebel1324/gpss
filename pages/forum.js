import Page from './components/layout';
import Hero from './components/hero';
import {
    Popup,
    Label,
    Input,
  Container,
  Divider,
  Icon,
  Header,
  Button,
} from 'semantic-ui-react'
import React from 'react'
import Link from 'next/link'
import endpoints from '../server/endpoints'
import fetch from 'isomorphic-unfetch'
import _ from 'lodash';
import { Cookies } from 'react-cookie'
import getInfo from './utils/loaddata'
import axios from 'axios';

const cookies = new Cookies();

// Load contents as scroll down
const InfBoard = ({children}) => {
    return (<Container>
        {children}
    </Container>)    
}

// A Post

const requestComment = (postID, text) => {
    const token = cookies.get('token')
    
    console.log({token, postID, text})
    axios.post(endpoints.doComment, {token, postID, text}).then(({data}) => {
        if (data.result == true) {
            window.location = "/forum"
        }
    })
}  
const idDisplayCount = 3;
class BoardPost extends React.Component {

    constructor(props) {
        super(props)
            this.state = {
                comments: [],
                commentText : ""
            }
            this.addComment = this.addComment.bind(this);
    }

    handleChange = (event, {name, value, checked}) => this.setState( { 
        [name] : value
    })

    detectEnter(event) {
        if (event.key === 'Enter') {
            requestComment(this.props.post_id, this.state.commentText)
            this.setState({commentText: ""})
        }
    }
    
    addComment() {  
        requestComment(this.props.post_id, this.state.commentText)
        this.setState({commentText: ""})
    }

    render() {
        const {id, title, content, likes, comments, who} = this.props

        const commentArray = this.state.comments.length > 0 ? this.state.comments : comments

        // Title, content, Likes, Comments
        return (<div className="post-box">
            <div className="board-title">{title || "제목 없음"}</div>   
            <div className="board-who">{who}</div>
            <div className="board-body">{content}</div>
            <div className="board-like">        
                <Popup
                    trigger={
                        <Button as='div' size='mini' labelPosition='right'>
                            <Button color='red' size='mini'>
                                <Icon name='heart' />
                                좋아요
                            </Button>
                            <Label as='a' basic color='red' pointing='left'>
                                {likes.length || 0}
                            </Label>
                        </Button>
                    }
                    content={() => {
                        if (likes.length > 0) {
                            let list = ""
                            
                            for (let index = 0; index < idDisplayCount; index++) {
                                const data = likes[index]
    
                                if (data) {
                                    list += data + ", ";
                                } else {
                                    break;
                                }
                            }
                            list = list.slice(0, -2);
        
                            if (likes.length > idDisplayCount) {
                                return `이 글을 ${list}님 외에 ${likes.length - idDisplayCount} 명이 좋아합니다.`
                            } else {
                                return `이 글을 ${list}님이 좋아합니다.`
                            }
                        } else {
                            return "아직 이 글을 좋아하는 사람이 없습니다."
                        }
                    }}
                    size='tiny'
                />
            </div>
            <Divider/>
            <div className='board-comments'>
                <Icon name='comment outline'></Icon>
                댓글 <span className='board-comments-count'>{commentArray.length}</span>
            </div>
            <div>
                {_.map(commentArray, data => (
                    <BoardComment
                        key={data.id}
                        player={data.name}
                        content={data.content}
                    />
                ))}
            </div>
            {
                this.props.me ? (
                        <div class="ui action input" style={{marginTop: "2rem", width: "100%"}}>
                            <Input style={{width: '100%'}}
                                type="text"
                                placeholder="댓글 내용"
                                name='commentText'
                                onChange={this.handleChange.bind(this)}
                                onKeyPress={this.detectEnter.bind(this)}
                                value={this.state.commentText}
                            />
                            <Button class="ui button" onClick={this.addComment.bind(this)}>
                                댓글 쓰기
                            </Button>
                        </div>
                ) : (<div></div>)
            }
        </div>)
    }
}

// A comment on post.
const BoardComment = ({player, content}) => {
    return (<div className="board-comment">
        <div className="board-comment-who">
            {player}
        </div>
        <div className="board-comment-body">
            {content}
        </div>
    </div>)
}

class Interaction extends React.Component {
    render() {
        return (
            <Container style={{marginBottom: "2rem", textAlign: "right"}}>
                <Button color='blue' href="/post">
                    New Post
                </Button>
            </Container>
        );  
    }
}

class Index extends React.Component {
    state = {
        playerInfo: null
    }

    static getInitialProps = async () => {
        var result = {}
        
        try {
            const fetchRequest = await fetch('http://localhost:8080/api/posts', {
               method: 'POST',
            })
            let jsonData = await fetchRequest.json()
        
            if (jsonData) {
                try {
                    for (var key in jsonData) {
                        const post = jsonData[key]
                        
                        try {
                            const fetchComments = await fetch(endpoints.comments, {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify({id: post.id})
                            })
                            
                            let comments = []
                            if (fetchComments.ok) {
                                const jsonComments = await fetchComments.json()
                                comments = jsonComments
                            }
    
                            jsonData[key].comments = comments
                        } catch (error) {
                            console.log(error)                  
                        }
                    }
                } catch (error) {
                    console.warn(error)
                }
            }
    
            result.posts = jsonData
        } catch (error) {
            console.log(error)   
        }
    
        return result
    }

    async componentWillMount() {
        const token = cookies.get('token')
    
        if (token) {
            const playerInfo = await getInfo(token)
            this.setState({playerInfo})
        }
    }

    render() {
        const props = this.props

        return (<Page playerInfo={this.state.playerInfo}>    
            <Hero>
                <Header as='h2'>
                    <Icon name='user'/>
                    <Header.Content>
                        Mini Forum    
                    <Header.Subheader>You can discuss about the game in here.</Header.Subheader>
                    </Header.Content>
                </Header>
            </Hero>
            <Interaction/>
            <InfBoard>
                {
                    _.map(
                        _.orderBy(props.posts, ['id'], ['desc'])    
                    , data => {
                        return (
                            <BoardPost
                                key={data.id}
                                post_id={data.id}
                                title={data.title}
                                content={data.content}
                                who={data.name}
                                who_id={data.player_id}
                                likes={[]}
                                comments = {data.comments} //해당 글에 달린 댓글들. MYSQL JOIN을 이용한다.
                                me = {this.state.playerInfo}
                            />
                        )
                    })

                    // TODO: 30개만 띄우고 그 이후는 INFINITIY SCROLL로 추가적으로 요청해서 글 올리기.
                    // TODO: 중복된 포스트는 띄우지 않는다.
                }
            </InfBoard>
        </Page>)
    }
}

export default Index;