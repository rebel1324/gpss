import Hero from './components/hero';
import Page from './components/layout';
import _ from 'lodash'
import {
  Container,
  Divider,
  Icon,
  Header,
  Table,
  Segment,
  Card,
  Input,
  TextArea,
  Form,
  Button,
  Message,
} from 'semantic-ui-react'

import {comma} from './utils/string';
import endpoints from '../server/endpoints'
import axios from 'axios'
import { Cookies } from 'react-cookie'
const cookies = new Cookies();
  
const validChecks = {}
class Index extends React.Component {
    state = {
        title: "",
        context: "",
        error: {
            title: null,
            context: null
        },
        errorMessage: ''
    }

    handleChange = (event, {name, value, checked}) => this.setState( { 
        [name] : value,
        error: {
            title: null,
            context: null
        },
        errorMessage: ''
    })

    handleSubmit = () => {
        const {errorMessage, email, password, passwordConfirm, name, tos} = this.state;
        
        try {
            for (let index = 0; index < validChecks.length; index++) {
                const errorInfo = validChecks[index](this)

                if (errorInfo) {
                    this.setState(errorInfo)
                    return false
                }
            }

            
            const token = cookies.get('token')
            const {context, title} = this.state
            axios.post(endpoints.doPost, {token, title, context}).then(({data}) => {
                if (data.result == true) {
                    window.location = "/forum"
                }
            }).catch(error => {
                console.warn(error)
            })
        } catch (error) {
            console.warn(error)
            this.setState({
                errorMessage: 'Sorry, Something went wrong.'
            })
        }
    }

    render = () => {
        const {errorMessage, email, password, passwordConfirm, name, tos} = this.state;

        return (
            <Page>
                <Hero>
                    <Header as='h2'>
                        <Icon name='user'/>
                        <Header.Content>
                            Write New Post
                        <Header.Subheader>Write a new post on the game forum.</Header.Subheader>
                        </Header.Content>
                    </Header>
                </Hero>
                <Container>
                    { 
                        (errorMessage != '') ? (<Message
                            header='Sorry, An error occured.'
                            error
                            content={errorMessage}
                        ></Message>) : undefined
                    }

                    <Form onSubmit={this.handleSubmit}>
                        <Form.Field required>
                            <Form.Input
                                label='Post Title'
                                type='text'
                                name='title'
                                value={email}
                                onChange={this.handleChange}
                                error={this.state.error.title}
                            ></Form.Input>
                        </Form.Field>
                        <Form.Field required>
                            <Form.Input
                                label='Context'
                                type='text'
                                name='context'
                                value={password}
                                onChange={this.handleChange}
                                error={this.state.error.context}
                                control={TextArea}
                                style={{minHeight: '20rem'}}
                            ></Form.Input>
                        </Form.Field>
                        <p>You can adjust the size of the Text Area by dragging the stripe on the right bottom of Text Area. 
                            Resize it when you're writing long post.
                        </p>
                        <Button primary icon='user' content='Submit' />
                    </Form>
                </Container> 
            </Page>
        );  
    }
}
export default Index;