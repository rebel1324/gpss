import Hero from './components/hero';
import Page from './components/layout';
import _ from 'lodash'
import {
  Container,
  Divider,
  Icon,
  Header,
  Table,
  Segment,
  Card,
  Input,
  Form,
  Message,
  Button,
} from 'semantic-ui-react'

import {comma} from './utils/string';
import endpoints from '../server/endpoints'
import fetch from 'isomorphic-unfetch'
import axios from 'axios'
import { Cookies } from 'react-cookie'

const cookies = new Cookies();

class Index extends React.Component {
    componentWillMount() {
        cookies.remove('token')
    }

    render() {
        return (
            <Page>
                <Container>
                    <p style={{textAlign: 'center'}}>
                        성공적으로 로그아웃 되었습니다.
                    </p>
                </Container>
            </Page>
        )
    }
}

export default Index;