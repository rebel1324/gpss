import Hero from './components/hero';
import Page from './components/layout';
import _ from 'lodash'
import {
  Container,
  Divider,
  Icon,
  Header,
  Table,
  Segment,
} from 'semantic-ui-react'

import {comma} from './utils/string';
import endpoints from '../server/endpoints'
import fetch from 'isomorphic-unfetch'
  
const dummyPlayerData = {
    id: 10,
    name: "Shrek",
    score: "100",
    woc: "weapon_pistol",
    kill: "1050",
    objective: "2020",
}
const playerList = [
    dummyPlayerData
]

const PlayerHeader = () => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>
                    Name
                </Table.HeaderCell>
                <Table.HeaderCell>
                    Score
                </Table.HeaderCell>
                <Table.HeaderCell>
                    WoC
                </Table.HeaderCell>
                <Table.HeaderCell>
                    Kill Count
                </Table.HeaderCell>
                <Table.HeaderCell>
                    Objectives
                </Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    )
}

const PlayerRow = ({playerData}) => {
    const {id, name, score, woc, kills, objectives} = playerData;

    return (
        <Table.Row key={id}>
        <Table.Cell>{name}</Table.Cell>
        <Table.Cell>{comma(score)}</Table.Cell>
        <Table.Cell>{woc}</Table.Cell>
        <Table.Cell>{comma(kills)}</Table.Cell>
        <Table.Cell>{comma(objectives)}</Table.Cell>
        </Table.Row>
    )
}

const Index = ({players}) => {
    return (
        <Page>
            <Hero>
                <Header as='h2'>
                    <Icon name='user'/>
                    <Header.Content>
                    Leaderboard
                    <Header.Subheader>People who played this game too much</Header.Subheader>
                    </Header.Content>
                </Header>
                <Segment basic>
                    <Header as='h4' floated='left'>
                    Ordered by scores
                    </Header>
                    <Header as='h4' floated='right'>
                    Total 4,123 players registered for this game.
                    </Header>
                </Segment>
            </Hero>
            <Container>
                <Table sortable celled fixed>
                    <PlayerHeader/>
                    <Table.Body>
                        {
                            _.map(players, data => (
                                <PlayerRow playerData={data}/>
                            ))
                        }
                    </Table.Body>
            </Table>
            </Container>
        </Page>
    )
}

Index.getInitialProps = async () => {
    let props = {}

    try {
        const fetchLeaderboard = await fetch(endpoints.leaderboard, {
           method: 'POST'
        })

        const jsonData = await fetchLeaderboard.json()
        props.players = jsonData;
    } catch (error) {
        console.warn(error)        
    }

    return props
}


export default Index;