import Page from './components/layout';
import Hero from './components/hero';
import {
  Container,
  Divider,
  Icon,
  Header,
  Label,
  Grid,
  Button,
  Image,
  Segment,
} from 'semantic-ui-react'
import _ from 'lodash';
import endpoints from '../server/endpoints'
import fetch from 'isomorphic-unfetch'
import {comma} from './utils/string'
import moment from 'moment'
import axios from 'axios'
import { Cookies } from 'react-cookie'
import getInfo from './utils/loaddata'

const cookies = new Cookies();
const profilePic = 'https://i.imgur.com/UjFDtxN.jpg'

const WrongProfile = () => {
    return (<div>
        <Segment placeholder>
            <Header icon>
            <Icon name='eye slash' />
                The ID you're looking for does not exists.<br/>
                Maybe try another id i guess?
            </Header>
        </Segment>
    </div>)
}

const PrivateSection = ({children}) => {
    return (<Segment placeholder>
        <Header icon>
        <Icon name='eye slash' />
            {children}
        </Header>
    </Segment>)
}

const VariableRow = ({title, body}) => {
    return (<div className="profile-var-row">
        <div className="profile-var-title">{title}</div>
        <div className="profile-var-body">{body}</div>
    </div>)
}

const resultConvert = {
    1: "WIN",
    2: "LOSE",
}

const resultConvertClass = {
    1: "win",
    2: "lose"
}

const PlayerProfile = ({player, matches}) => {
    const {name} = player
    let matchesDOM = []
    matches.forEach(match => {
        const {
            playerID, joinTime, quitTime, kills, powerups, bombs, score
        } = match

        if (quitTime) {
            matchesDOM.push((
                <div className="profile-match-row" key={joinTime}>
                    <div className="profile-match-mode">{`MATCH COMPLETE`}</div>
                    <div className="profile-match-date">{(moment(quitTime).format(`LLLL`))}</div>
                    <div>Kills: {kills}</div>
                    <div>Powerups: {powerups}</div>
                    <div>Bombs: {bombs}</div>
                    <div>Score: {score}</div>
                </div>
            ))
        } else {
            matchesDOM.push((
                <div className="profile-match-row" key={joinTime}>
                    <div className="profile-match-mode">{`MATCH ONGOING`}</div>
                    <div className="profile-match-date">{(moment(joinTime).format(`LLLL`))}</div>
                    <div>Kills: {kills}</div>
                    <div>Powerups: {powerups}</div>
                    <div>Bombs: {bombs}</div>
                    <div>Score: {score}</div>
                </div>
            ))
        }
    });

    return (<div>
        <Container className="profile-margin">
            <Segment basic>
                <Grid>
                    <Grid.Column style={{width: '128px'}}>
                        <Image src={profilePic} size='small' centered circular />
                    </Grid.Column>
                    <Grid.Column width={9}>
                    <div className="profile-player-clan">
                        {'No Clan'}
                    </div>
                    <div className="profile-player-name">
                        {name}
                    </div>
                    <div className="profile_row" style={{'marginTop': '1.5rem'}}>
                    <Button basic size='tiny' color='red'>
                        <Icon name='heart' />
                        Favorite
                    </Button>
                    <Button basic size='tiny' color='orange'>
                        <Icon name='exclamation' />
                        Report
                    </Button>
                    </div>
                </Grid.Column>
                </Grid>
            </Segment>

            <Segment className="profile-box">
                <VariableRow title={`Total Kills`} body={comma(player.kills)}/>
                <VariableRow title={`Total Objectives`} body={comma(player.objectives)}/>
                <VariableRow title={`Total Score`} body={comma(player.score)}/>
                <VariableRow title={`Total Speed Powerups`} body={comma(player.item_speed)}/>
                <VariableRow title={`Total Power Powerups`} body={comma(player.item_power)}/>
                <VariableRow title={`Total Bomb Powerups`} body={comma(player.item_bomb)}/>
                <VariableRow title={`Total Fly Powerups`} body={comma(player.item_fly)}/>
                <VariableRow title={`Total Placed Bombs`} body={comma(player.bomb_placed)}/>
            </Segment>

            <Segment className="profile-box">
                {
                    matchesDOM // idk why but somehow lodash.map is not working
                }
            </Segment>
        </Container>
    </div>)    
}

class Index extends React.Component {
    // TODO: Make transition more smoother
    // HACK: Clientside Token hack - token session must sustain both of the server.
    // HACK: Need to learn the method of sustaining session all together.

    state = {
        playerInfo: null
    }
    
    static getInitialProps = async ({ query: { id } } ) => {
        let result = {}
    
        try {
            if (id) {
                const fetchRequest = await axios.post(endpoints.player, {
                    id: id
                })
                const jsonData = fetchRequest.data
                if (jsonData) {
                    const {player, matches} = jsonData
        
                    if (player && player[0]) {
                        result.player = player[0]; // The player should be unique. 
                        result.matches = matches;
                        
                        result.isValid = true;
                    } else {
                        result.isValid = false
                        return result;
                    }
                } else {
                    result.isValid = false;
                }
            } else {
                result.redirect = true
            }
        } catch (error) {
            console.warn(error)   
            result.isValid = false;     
        }
    
        return result
    }

    async componentWillMount() {
        const token = cookies.get('token')

        if (token) {
            const playerInfo = await getInfo(token)

            this.setState({playerInfo})
            if (playerInfo && this.props.redirect) {
                if (playerInfo.id) {
                    window.location.search = `id=${playerInfo.id}`
                }
            } 
        }
    }

    render() {
        const {player, matches, isValid} = this.props

        const customDOM = (isValid ? (<PlayerProfile player={player} matches={matches}/>) : (<WrongProfile/>))
        return (<Page playerInfo={this.state.playerInfo}>     
            {customDOM}
        </Page>)
    }
}


export default Index;