import React from 'react';
import _ from 'lodash'
import ServiceHeader from 'components/ServiceHeader'
import ServiceFooter from 'components/ServiceFooter';
import {
  Container,
  Divider,
  Icon,
  Header,
  Table,
  Segment,
} from 'semantic-ui-react'
import { host } from 'libraries/Host.js'

function comma(str) {
  return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default class PageLeaderboard extends React.Component {
  constructor() {
    super()
    this.state = {
      players: []
    }
  }

  componentDidMount() {
    // reroute stuffs later. maybe ngnix?

    fetch(`${host}:8080/api/leader`, { 
      method: 'POST',
    }).then(response => response.json())
    .then(data => this.setState({ players: data }));
  }

  render() {
    return (
      <div>
        <ServiceHeader title="Leaderboard" active="leaderboard"/ >
        <Container style={{ marginTop: '5em' }}>
  
          <Header as='h2'>
            <Icon name='user'/>
            <Header.Content>
              Leaderboard
              <Header.Subheader>People who played this game too much</Header.Subheader>
            </Header.Content>
          </Header>
          <Divider />
          
          <Segment basic>
            <Header as='h4' floated='left'>
              Ordered by scores
            </Header>
            <Header as='h4' floated='right'>
              Total 4,123 players registered for this game.
            </Header>
          </Segment>
          <Table sortable celled fixed>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>
                  Name
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Score
                </Table.HeaderCell>
                <Table.HeaderCell>
                  WoC
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Kill Count
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Objectives
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {
              _.map(this.state.players, data => (
                <Table.Row key={data.id}>
                  <Table.Cell>{data.name}</Table.Cell>
                  <Table.Cell>{comma(data.score)}</Table.Cell>
                  <Table.Cell>{data.woc}</Table.Cell>
                  <Table.Cell>{comma(data.kill)}</Table.Cell>
                  <Table.Cell>{comma(data.objective)}</Table.Cell>
                </Table.Row>
              ))
              }
            </Table.Body>
          </Table>
        </Container>
        <ServiceFooter/>
      </div>
    );
  }
}