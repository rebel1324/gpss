import React from 'react';
//import _ from 'lodash' // Somebody once told me it's some utility
import ServiceHeader from 'components/ServiceHeader'
import ServiceFooter from 'components/ServiceFooter';
import {
  Container,
  Divider,
  Button,
  Grid,
  Icon,
  Header,
  Image,
  Label,
  Segment,
} from 'semantic-ui-react'
import './GameProfie.css'

const profilePic = 'https://i.imgur.com/UjFDtxN.jpg'

export default class PageProfile extends React.Component {
  render() {
    return (
      <div>
        <ServiceHeader title="Profile" active="profile"/ >
        <Container style={{ marginTop: '5em' }}>
  
          <Header as='h2'>
            <Icon name='user'/>
            <Header.Content>
              Profile
              <Header.Subheader>Check your current status in the game</Header.Subheader>
            </Header.Content>
          </Header>
          <Divider />

          <Segment basic>
            <Label style={{'backgroundColor': '#9b59b6', 'color': '#fafafa'}} inverted="true">
              <Icon name='heart' />
              Founder
            </Label>
            <Label style={{'backgroundColor': '#2ecc71', 'color': '#fafafa'}} inverted="true">
              <Icon name='check' />
              Verified
            </Label>
            <Label style={{'backgroundColor': '#e74c3c', 'color': '#fafafa'}} inverted="true">
              <Icon name='exclamation' />
              Suspended
            </Label>
            <Label style={{'backgroundColor': '#3498db', 'color': '#fafafa'}} inverted="true">
              <Icon name='trophy' />
              Ranked
              <Label.Detail>1st</Label.Detail>
            </Label>
          </Segment>

          <Segment basic>
            <Grid>
              <Grid.Column style={{width: '128px'}}>
                <Image src={profilePic} size='small' centered circular />
              </Grid.Column>
              <Grid.Column width={9}>
                <div className="profile_team profile_row">[IMGT] I AM GROOT</div>
                <div className="profile_name profile_row">
                  Black Tea Za rebel1324 
                </div>
                <div className="profile_row" style={{'marginTop': '1.5rem'}}>
                  <Button basic size='tiny' color='red'>
                    <Icon name='heart' />
                    Favorite
                  </Button>
                  <Button basic size='tiny' color='orange'>
                    <Icon name='exclamation' />
                    Report
                  </Button>
                </div>
                <div className="profile_lastseen profile_row">
                  Most Recent Played Game: {`2 days ago (Victory - Solo)`}
                </div>
              </Grid.Column>
            </Grid>
          </Segment>

          <Segment placeholder>
            <Header icon>
              <Icon name='eye slash' />
              This user's statistic is private
            </Header>
          </Segment>

          <Segment placeholder>
            <Header icon>
              <Icon name='eye slash' />
              This user's recent play record is private
            </Header>
          </Segment>
        </Container>
        <ServiceFooter/>
      </div>
    );
  }
}