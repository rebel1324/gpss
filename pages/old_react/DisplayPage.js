import React, { Component } from 'react'
import { Grid, Divider, Header, Segment, Container, Button, Card, Image, Icon } from 'semantic-ui-react'
import FastLazyHero from 'components/fast-hero'

import resDev01 from 'res/img/dev01.jpg'
import resDev02 from 'res/img/dev02.jpg'
import resSample from 'res/img/sample01.jpg'

const src = 'https://react.semantic-ui.com/images/wireframe/square-image.png'
class HeaderHero extends Component {
  render() {
    return (
      <FastLazyHero opacity={.4} color="black" imageSrc={resSample} minHeight='35rem'>
        <Image src={src} size='small' centered circular />
        <Header as='h1' inverted>Project Handshake</Header>
        <Button inverted basic>
          <Icon name='download'/>
          Download Game
        </Button>
      </FastLazyHero>
    )
  }
}

class RepoHero extends Component {
  render() {
    return (
      <div>
        <FastLazyHero minHeight='20rem' color="#fafafa">
          <Container textAlign='left'>
          <Header
            as='h2'
            content='Multiplayer Service'
            subheader='Good way to Start Unreal Engine 4 Multiplayer Service.'
          />

          <p>Today's Gaming Industry is all about Multiplayer. Even Sigleplayer Games needs Internet Connection for better expereinces.</p>
          <p>I know that not all of people are good at making multiplayer game. Even don't get where to start.</p>
          <p>When you don't know where to go, This is one good choice to start up your own game with some web service.</p>
          </Container>
        </FastLazyHero>
        <FastLazyHero minHeight='20rem' color="#eaeaea">
          <Container textAlign='left'>
          <Header
            as='h2'
            content='Rich Contents'
            subheader='From Client to Master Server.'
          />

          <p>You have a lot of questions. For that, Rich Open Source Repositories has Game Client, Game Server and Master Server.</p>
          <p>This Assets are Powered by most-popular frameworks, libraries and technologies in 2018. By looking at the code and documents, You'll set what you need for your own Future Multiplayer Game.</p>
          </Container>
        </FastLazyHero>
        <FastLazyHero minHeight='20rem' color="#fafafa">
          <Container textAlign='left'>
          <Header
            as='h2'
            content='Open Source'
            subheader='All Source Code is public with MIT License.'
            />

          <p>All of these works are released in public. You can fork and branch to your own game with GitHub</p>
          <p>Also with MIT License, You don't have to worry about license and stuffs. All you need is code.</p>
          <p>If you're pleased with the contents, feel free to leave stars and watch to our GitHub Repository.</p>
          </Container>
        </FastLazyHero>
      </div>
    )
  }
}

class MediaHero extends Component {
  render() {
    return (
      <FastLazyHero minHeight='50rem' color="#fafafa">
        <Header
          as='h2'
          content='Media'
          subheader='This is what you get'
        />

        <Container>
          <Segment vertical>
            <Image.Group size='small'>
              <Image src={src} />
              <Image src={src} />
              <Image src={src} />
            </Image.Group>
          </Segment>
        </Container>
      </FastLazyHero>
    )
  }
}

class TeamCard extends Component {
  render() {
    let gitDOM = (<div></div>)
    let twitterDOM = (<div></div>)

    if (this.props.twitter) {
      twitterDOM = (<a href={this.props.twitter}><Icon size='large' name='twitter' /> </a>)
    }
    if (this.props.github) {
      gitDOM = (<a href={this.props.github}><Icon size='large' name='github' /></a>)
    }

    return (
      <Card>
        <Image src={this.props.pic} />
        <Card.Content>
          <Card.Header>{this.props.name}</Card.Header>
          <Card.Meta>
            <span className='date'>{this.props.role}</span>
          </Card.Meta>
          <Card.Description>
            <p>{this.props.desc}</p>
          </Card.Description>
        </Card.Content>
        
        <Card.Content extra>
          {gitDOM}
          {twitterDOM}
        </Card.Content>
      </Card>
    )
  }
}

const teamList = [
  {
    pic: resDev01,
    name: "Kyu Yeon \"rebel\" Lee",
    role: "Project Leader / Server Programming",
    desc: "The mememan who studied Computer Science and being bedroom programmer for 4 years. Tried to make some games but ended up making game framework and libaries.",
    github: "http://github.com/rebel1324",
    twitter: "https://twitter.com/_rebel1324",
  },
  {
    pic: resDev02,
    name: "Sang Hwa Lee",
    role: "QA / Client Progamming",
    desc: "True Anime Boi who just want to make something good. Bretty good at making game clients. ",
    github: "",
    twitter: "https://twitter.com/sleuthl007",
  },
]

class TeamHero extends Component {
  render() {
    let genDOM = []
    teamList.forEach((data) => {
      genDOM.push((
        <TeamCard 
          pic={data.pic}
          name={data.name}
          quote={data.quote}
          role={data.role}
          desc={data.desc}
          history={data.history}
          key={data.name}
          github={data.github}
          twitter={data.twitter}
        />
      ))
    })

    return (
      <FastLazyHero minHeight='50rem' color="#eaeaea">
        <Header
          as='h2'
          content='Contributors'
          subheader='We make this better by the time'
        />
        <Card.Group>
          {genDOM}
        </Card.Group>
      </FastLazyHero>
    )
  }
}

class CommunicateHero extends Component {
  render() {
    return (
      <FastLazyHero minHeight='20rem' color="#eaeaea">
        <Container>
          <Grid stackable>
            <Grid.Column floated='left' width={7}>
              <Header textAlign='left' as='h2' icon='question circle outline' content='Join Discussion' subheader='Share your ideas and thoughts'/>
            </Grid.Column>
            <Grid.Column floated='right' width={3}>
              <Button href='/leaderboard'>Join</Button>
            </Grid.Column>
          </Grid>

          <Divider horizontal>Or</Divider>

          <Grid stackable>
            <Grid.Column floated='left' width={7}>
              <Header textAlign='left' as='h2' icon='users' content='Leaderboard' subheader="Check who's winning the game!"/>
            </Grid.Column>
            <Grid.Column floated='right' width={3}>
              <Button href='/forum'>Join</Button>
            </Grid.Column>
          </Grid>
        </Container>
      </FastLazyHero>
    )
  }
}

export default class PageDisplay extends Component {
  render() {
    return (
      <div>
        <HeaderHero>
        </HeaderHero>
        <RepoHero>
        </RepoHero>
        <TeamHero>
        </TeamHero>
        <MediaHero>
        </MediaHero>
        <CommunicateHero>
        </CommunicateHero>
      </div>
    );
  }
}
//new page  
// address/game/login -- 로그인 (소셜 로그인이나 아니면 간단한 로그인 시스템)
// address/game/profile -- 프로필 (게임 동기화 정보 / 전적... etc.)
// address/game/leaderboard -- 리더보드
// address/game/board -- 게임 관련 방명록