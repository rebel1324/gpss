import Hero from './components/hero';
import Page from './components/layout';
import _ from 'lodash'
import {
  Container,
  Divider,
  Icon,
  Header,
  Table,
  Segment,
  Card,
  Input,
  Form,
  Message,
  Button,
} from 'semantic-ui-react'

import endpoints from '../server/endpoints'
import axios from 'axios'

class Index extends React.Component {
    componentDidMount() {
        setTimeout(() => {
            window.location.href = '/'
        }, 1500);
    }

    render() {
        return (
            <Page>
                <Container>
                    <div style={{
                        marginTop: '10vh'
                    }}></div>
                    <div style={{
                        textAlign: 'center'
                    }}>
                        <p>
                            Sucessfully registered the account!
                        </p>
                        <p>
                            Redirecting back to main page..
                        </p>
                        <p>
                            <a href="/">Click this link if you don't get redirected.</a>
                        </p>
                    </div>
                </Container>
            </Page>
        )
    }
}

export default Index;