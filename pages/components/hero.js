import { Container } from "semantic-ui-react";

export default ({children, className}) => {
    return (<div className={'mini-hero ' + className}>
        <Container>
            {children}
        </Container>
    </div>)
}