module.exports = {
    founder: (
    <Label style={{'backgroundColor': '#9b59b6', 'color': '#fafafa'}} inverted="true">
        <Icon name='heart' />
            Founder
    </Label>),  
    verified: (
    <Label style={{'backgroundColor': '#2ecc71', 'color': '#fafafa'}} inverted="true">
        <Icon name='check' />
            Verified
    </Label>),  
    suspended: (
    <Label style={{'backgroundColor': '#e74c3c', 'color': '#fafafa'}} inverted="true">
        <Icon name='exclamation' />
            suspended
    </Label>),  
    ranked: (
    <Label style={{'backgroundColor': '#3498db', 'color': '#fafafa'}} inverted="true">
        <Icon name='trophy' />
            Founder
    </Label>)
}