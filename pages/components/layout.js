import Head from 'next/head';
import ServiceHeader from './header'
import ServiceFooter from './footer'

export default ({children, playerInfo}) => {
    return (
        <div>
            <Head>
                <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css" rel="stylesheet"></link>
                <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" rel="stylesheet"></link>
                <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
                <meta charSet="utf-8"></meta>
            </Head>
            <style>{`
                body {
                    background: #fafafa;
                }
                .content-margin {
                    margin-top: 39.6px;
                }
                .mini-hero {
                    padding: 50px;
                }
                .hero-background {
                    text-align: center;
                    background: #e74c3c;
                }
                .board-title {
                    font-weight: bold;
                    font-size: 1.5em;
                    margin-botton: 0;
                }
                .board-who {
                    font-size: 0.9rem;
                    margin-bottom: 0.5rem;
                }
                .board-body {
                    font-size: 1rem;
                    margin-bottom: 1rem;
                }
                
                .profile-box {
                    background: white;
                    margin-bottom: 1.5rem;
                    padding: 2rem;
                    -webkit-box-shadow: 0px 5px 16px -4px rgba(0,0,0,0.6);
                    -moz-box-shadow: 0px 5px 16px -4px rgba(0,0,0,0.6);
                    box-shadow: 0px 5px 7px -2px rgba(0,0,0,0.6);
                }
                .profile-var-title {
                    font-size: 1.4rem;
                    margin-bottom: 0.2rem;
                    font-weight: bold;
                }
                .profile-var-body {
                    font-size: 1rem;
                }
                .profile-var-row {
                    margin-bottom: 10px;
                }
                .profile-player-name {
                    font-size: 2rem;
                    font-weight: bold;
                }
                .profile-player-clan {
                    font-size: 1.25rem;
                }

                .profile-match-row {
                    margin-bottom: 1rem;
                }
                .profile-match-mode {
                    font-size: 1.5rem;
                }
                .profile-match-result {
                    font-size: 1.2rem;
                }
                .profile-match.result.win {
                    color: blue !important;
                }
                .profile-match.result.lose {
                    color: red !important;
                }
                .profile-match-date {

                }
                .profile-margin {
                    margin-top: 2rem !important;
                }

                .post-box {
                    background: white;
                    margin-bottom: 1.5rem;
                    padding: 2rem;
                    -webkit-box-shadow: 0px 5px 16px -4px rgba(0,0,0,0.6);
                    -moz-box-shadow: 0px 5px 16px -4px rgba(0,0,0,0.6);
                    box-shadow: 0px 5px 7px -2px rgba(0,0,0,0.6);
                }
                .board-like {
                    text-align: right;
                }
                .board-comment {
                    margin-top: 0.5rem;
                }
                .board-comment-who {
                    font-weight: bold;
                }
                .board-comment-body {
                    
                }
                .board-comments {
                    font-size: 1rem;
                    font-weight: bold;
                }
                .board-comments-count {
                    font-size: 0.85rem;
                }

            `}</style>
            <body className="content-margin">
                <ServiceHeader playerInfo={playerInfo}></ServiceHeader>
                {children}
                <ServiceFooter playerInfo={playerInfo}></ServiceFooter>
            </body>
        </div>
    )
}