import React from 'react';
import {
    Container,
    Menu,
} from 'semantic-ui-react'

export default class ServiceHeader extends React.Component {
	render() {
		const {playerInfo} = this.props

		return (
		<div>
			<Menu fixed='top'>
			<Container>
				<Menu.Item header>
					Project Handshake
				</Menu.Item>
				<Menu.Item as='a' href='/' name='main' active={this.props.active === 'main'}/>
				<Menu.Item as='a' href='http://localhost:5000/' name='game'/>
				<Menu.Item as='a' href='/leaderboard' name='leaderboard' active={this.props.active === 'leaderboard'}/>
				<Menu.Item as='a' href='/forum' name='forum' active={this.props.active === 'forum'}/>
				<Menu.Item as='a' href='/profile' name='profile' active={this.props.active === 'profile'}/>
				<Menu.Menu position='right'> 
					{ playerInfo ? (
						<Menu.Item 
							as='a'
							name='logout'
							href='/logout'
						/>
						)
						:
						(
						<Menu.Item 
							as='a'
							name='login'
							href='/login'
						/>
						)
					}
				</Menu.Menu>
			</Container>
			</Menu>
		</div>
		);
  	}
}