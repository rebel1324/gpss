import React from 'react';
import {
    Container,
    List,
    Segment,
} from 'semantic-ui-react'

export default class ServiceFooter extends React.Component {
  render() {
    return (
      <div>
        <Segment vertical size='mini' style={{ margin: '5em 0em 0em', padding: '5em 0em' }}>
        <Container textAlign='center'>
            <List horizontal divided link>
            <List.Item as='a' href='#'>
                About Us
            </List.Item>
            <List.Item as='a' href='#'>
                Contact Us
            </List.Item>
            <List.Item as='a' href='#'>
                Terms and Conditions
            </List.Item>
            <List.Item as='a' href='#'>
                Privacy Policy
            </List.Item>
            </List>
            <p>
                2018-2019 Contributed by Kyu Yeon 'rebel' Lee && Lee Sang Hwa, Rocket Team.
            </p>
        </Container>
        </Segment>
      </div>
    );
  }
}