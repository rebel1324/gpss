import React from 'react'
import LazyHero from 'react-lazy-hero'
import styled from 'styled-components';
import Color from 'color';

const Cover = styled.div `
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
`;

const Root = styled.div `
    position: relative;
    min-height: ${props => props.minHeight};
`;

const Img = styled(Cover)`
    background-attachment: ${props => (props.isFixed ? 'fixed' : 'scroll')};
    background-image: url(${(props => props.src)});
    background-position: center;
    background-repeat: no-repeat;
    background-size: ${props => (props.width ? `${props.width}px ${props.height}px` : 'cover')};
    opacity: 1;
    transition-duration: ${props => `${props.transitionDuration}ms`};
    transition-property: opacity;
    transition-timing-function: ${props => props.transitionTimingFunction};
`;

const Overlay = styled(Cover)`
    display: flex;
    justify-content: ${props => (props.isCentered ? 'center' : 'flex-start')};
    align-items: ${props => (props.isCentered ? 'center' : 'stretch')};
    text-align: ${props => (props.isCentered ? 'center' : 'left')};
    background-color: ${props => Color(props.color).alpha(props.opacity).rgb().string()};
`;

class FastLazyHero extends LazyHero {
    render() {
        const {
            backgroundDimensions,
            backgroundPositionY
        } = this.state;

        return ( <
            Root className = {
                this.props.className
            }
            innerRef = {
                (r) => {
                    this.ref = r;
                }
            }
            minHeight = {
                this.props.minHeight
            }
            style = {
                this.props.style
            } >
            <
            Img height = {
                backgroundDimensions && backgroundDimensions.height
            }
            isVisible = {
                this.state.image && this.state.isInViewport
            }
            isFixed = {
                this.props.isFixed || this.props.parallaxOffset > 0
            }
            src = {
                this.props.imageSrc
            }
            style = {
                {
                    backgroundPositionY
                }
            }
            transitionDuration = {
                this.props.transitionDuration
            }
            transitionTimingFunction = {
                this.props.transitionTimingFunction
            }
            width = {
                backgroundDimensions && backgroundDimensions.width
            }
            /> <
            Overlay color = {
                this.props.color
            }
            isCentered = {
                this.props.isCentered
            }
            opacity = {
                this.props.opacity
            } >
            {
                this.props.children && < div > {
                    this.props.children
                } < /div>} <
                /Overlay> <
                /Root>
            );
        }
    }

    export default FastLazyHero;