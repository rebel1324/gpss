// well it works anyway lmao
import PageForum from './GameForum'
import PageDisplay from './DisplayPage'
import PageLeaderboard from './GameLeaderboard'
import PageProfile from './GameProfile'
import PageLogin from './GameLogin'

export {
    PageForum,
    PageDisplay,
    PageLeaderboard,
    PageProfile,
    PageLogin
}