const router = require('express').Router();
const connection = require('../server/database');
const queries = require('../server/queries') 
const cors = require('cors')

const jwt = require('jsonwebtoken')
const jwtConfig = require('../passport')
const extractionRegex = /[\s\w]+\'([!-~가-힝]+)\'[\s\w]+\'([!-~가-힝]+)'/g

router.use(cors())

const extractDuplicateInformation = (errorMessage, indentifier) => {
    if (errorMessage) {
        const parsedData = errorMessage.split(extractionRegex)

        return {
            key: parsedData[2],
            value: parsedData[1]
        }
    } else {
        return null
    }
}

router.post('/register', (request, response) => {
    // TODO: Store hashed passwords.
    try {
        if (request.body) {
            const {email, password, name} = request.body 
            
            if (email && password && name) {
                // HACK: Possibly going to cause database failure: in condition of heavy load of DBMS - LAST_INSERT_ID() will fuck up so hard.

                connection.query(queries.registerPlayer, [name], (error, result, fields) => {
                    if (!error) {
                        connection.query(queries.registerAccount, [email, password], (error, result, fields) => {
                            if (error) {
                                // TODO: Remove registered player account from database to eliminate junk records.
                                connection.query(queries.removeAccount, (error) => {
                                    if (error) {
                                        console.warn("REMOVING PLAYER ACCOUNT FAILED! - CHECK HACK!")
                                    }
                                })
                                
                                const errorData = extractDuplicateInformation(error.sqlMessage)

                                response.status(200)
                                response.send({
                                    failureCode: 2,
                                    failed: true, 
                                    failedValue: errorData ? errorData.value : undefined,
                                    failedKey: errorData ? errorData.key : undefined
                                })
                            } else {
                                console.log('New player registered.')
                                response.status(200)
                                response.send({
                                    failed: false
                                })
                            }           
                        })
                    } else {
                        const errorData = extractDuplicateInformation(error.sqlMessage, 1)

                        response.send({
                            failureCode: 1,
                            failed: true, 
                            failedValue: errorData ? errorData.value : undefined,
                            failedKey: errorData ? errorData.key : undefined
                        })
                    }
                })
                
            }
        } else {
            response.status(200)
            response.send({
                failureCode: 0,
                failed: true
            })
        }
    } catch (error) {
        console.warn(error)
        response.status(200)
        response.send({
            failureCode: -1,
            failed: true
        })
    }
})

router.post('/ping', (request, response) => {
    // TODO: Send authed user information for service. 
    // FUTURE: Find better way to do this.

    if (request.body) {
        const {token} = request.body
        
        response.send( 
            jwt.verify(token, jwtConfig.secret)
        )
    } else {
        response.send({})
    }
})

router.post('/logout', (request, response, next) => {
    // TODO: Logout JWT Token Invalidate
})

router.post('/', (request, response, next) => {
    const {email, password} = request.body
    //console.log(request.body)

    if (!email || !password) {
        response.status(400);
        response.send("Invalid Response");
        return
    }

    connection.query(queries.login, [email, password], (error, result, fields) => {
        try {
            if (error) {
                console.warn(error)
                response.status(500)
                response.send(error)
            } else {
                if (result.length == 0) {
                    response.status(500)
                    response.send("Unauthorized")
                } else {
                    const {name, score, id, item_speed, item_power, item_bomb, item_fly, bomb_placed} = result[0]
                    const webToken = jwt.sign({
                        name: name,
                        score: score,
                        id: id,
                        item_speed: item_speed,
                        item_power: item_power,
                        item_bomb: item_bomb,
                        item_fly: item_fly,
                        bomb_placed: bomb_placed
                    }, jwtConfig.secret, {
                        expiresIn: "24h",
                    })
    
                    response.cookie("user", webToken)
                    response.json({
                        token: webToken
                    })
                }
            }
        } catch (error) {
            console.warn(error)
            response.status(500)
            response.send(error)
        }
    })
})

module.exports = router;