const router = require('express').Router();
const connection = require('../server/database');
const queries = require('../server/queries') 
const cors = require('cors')

const jwt = require('jsonwebtoken')
const jwtConfig = require('../passport')

/*
    Fetch the Informations.
*/
router.use(cors())

router.post('/leaderboard', (req, res) => {
    new Promise((resolve, reject) => {
        connection.query(queries.leaderboard, (error, result, fields) => {
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
        })
    }).then(data => {
        res.send(data)
    }).catch(error => {
        res.status(400)
        res.send(error)
    })
})

router.post('/matches', (req, res) => {
    res.send("fucker")
})

router.post('/posts', (req, res) => {
    new Promise((resolve, reject) => {
        connection.query(queries.posts, [], (error, result, fields) => {
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
        })
    }).then(data => {
        res.send(data)
    }).catch(error => {
        res.send(error)
        res.status(400)
    })
})

router.post('/comments', (req, res) => {
    const data = req.body;

    if (data) {
        const { id } = data;
    
        new Promise((resolve, reject) => {
            connection.query(queries.comments, [id], (error, result, fields) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(result)
                }
            })
        }).then(data => {
            res.send(data)
        }).catch(error => {
            res.send(error)
            res.status(400)
        })   
    } else {
        res.status(400)
        res.send("error")
    }
})

router.post('/likes', (req, res) => {
    const data = req.body;
    const { id } = data;

    new Promise((resolve, reject) => {
        connection.query(queries.likes, [id], (error, result, fields) => {
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
        })
    }).then(data => {
        res.send(data)
    }).catch(error => {
        res.send(error)
        res.status(400)
    });
})

router.post('/profile', (req, res) => {
    const data = req.body;

    if (data) {
        const { id } = data;

        if (id) {
            const playerInformation = new Promise((resolve, reject) => {
                connection.query(queries.profile, [id], (error, result, fields) => {
                    if (error) {
                        reject(error)
                    } else {
                        resolve(result)
                    }
                })    
            })
            const playerMatches = new Promise((resolve, reject) => {
                connection.query(queries.matches, [id], (error, result, fields) => {
                    if (error) {
                        reject(error)
                    } else {
                        resolve(result)
                    }
                })    
            })
            
            Promise.all([playerInformation, playerMatches]).then(data => {
                res.send({
                    player: data[0],
                    matches: data[1]
                })
            }).catch(error => {
                console.warn(error)
                res.status(400);
                res.send(error)
            })
        } else {
            res.status(400)
            res.send({})
        }
    } else {
        res.status(400)
        res.send({})
    }
})

/*
    Submit the Informations.
*/
router.post('/dolike', (req, res) => {
    res.send("")
})

router.post('/docomment', (req, res) => {
    try {
        const {token, postID, text} = req.body        
        const {id} = jwt.verify(token, jwtConfig.secret)

        connection.query(queries.addComment, [id, postID, text], (error, result, fields) => {
            if (error) {
                throw error
            }

            res.send({result: true})
        })
    } catch(error) {
        res.send({result: false, error: 400, errorMessage: "Something went wrong!"})
        res.status(400) // something went wrong!
    }
})

router.post('/dopost', (req, res) => {
    try {
        const {token, title, context} = req.body
        const {id} = jwt.verify(token, jwtConfig.secret)

        connection.query(queries.addPost, [id, title, context], (error, result, fields) => {
            if (error) {
                throw error
            }

            res.send({result: true})
        })
    } catch(error) {
        res.send({result: false, error: 400, errorMessage: "Something went wrong!"})
        res.status(400) // something went wrong!
    }
})

module.exports = router;
