const mysql = require('mysql');
const connection = mysql.createConnection({
    host:       '127.0.0.1',
    user:       'root',
    password:   '',
    database:   'handshake'
})

connection.connect(error => {
    if (error) {
        console.error(error);
        return
    }

    console.log("[DB] The database connection has been established.")
})

module.exports = connection;
