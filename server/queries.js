module.exports = {
    posts: `SELECT 
    forum_posts.id, forum_posts.player_id, forum_posts.title, forum_posts.content, player.name
    FROM forum_posts
    INNER JOIN player ON forum_posts.player_id=player.id`,

    post: `SELECT 
    forum_posts.id, forum_posts.player_id, forum_posts.title, forum_posts.content, player.name
    FROM forum_posts
    INNER JOIN player ON forum_posts.player_id=player.id
    WHERE forums_posts.id=?`,

    comments: `SELECT
    forum_comments.id, forum_comments.player_id, forum_comments.content, player.name
    FROM forum_comments
    INNER JOIN player ON forum_comments.player_id=player.id
    WHERE forum_comments.post_id=?`,

    addComment: `INSERT INTO forum_comments
    (\`player_id\`, \`post_id\`, \`content\`) VALUES (?, ?, ?)`,

    addPost: `INSERT INTO forum_posts
    (\`player_id\`, \`title\`, \`content\`) VALUES (?, ?, ?)`,

    likes: `SELECT
    forum_likes.id, forum_likes.player_id
    FROM
    forum_likes
    WHERE
    forum_likes.post_id=?`,

    matches: `SELECT * FROM matches_list
    WHERE matches_list.playerID=?
    ORDER BY matches_list.joinTime
    DESC`,

    profile: `SELECT *
    FROM player
    WHERE player.id=?`,

    placeBomb: `UPDATE player SET player.bomb_placed = player.bomb_placed + 1, player.score = player.score + 10 WHERE player.id=?`,

    addSpeed: `UPDATE player SET player.item_speed = player.item_speed + 1 WHERE player.id=?`,
    addPower: `UPDATE player SET player.item_power = player.item_power + 1 WHERE player.id=?`,
    addBomb: `UPDATE player SET player.item_bomb = player.item_bomb + 1 WHERE player.id=?`,
    addFly: `UPDATE player SET player.item_fly = player.item_fly + 1 WHERE player.id=?`,
    addKill: `UPDATE player SET player.kills = player.kills + 1 WHERE player.id=?`,

    startMatch: `INSERT INTO matches_list (\`playerID\`, \`joinTime\`)
    VALUES (?, NOW())`,
    endMatch: `UPDATE matches_list
    SET matches_list.quitTime = NOW()
    WHERE matches_list.playerID=? AND matches_list.quitTime IS NULL`,
    addMatchBombs: `UPDATE matches_list
    SET matches_list.bombs = matches_list.bombs + 1
    WHERE matches_list.playerID=? AND matches_list.quitTime IS NULL
    LIMIT 1`,
    addMatchPowerups: `UPDATE matches_list
    SET matches_list.powerups = matches_list.powerups + 1
    WHERE matches_list.playerID=? AND matches_list.quitTime IS NULL
    LIMIT 1`,
    addMatchKills: `UPDATE matches_list
    SET matches_list.kills = matches_list.kills + 1
    WHERE matches_list.playerID=? AND matches_list.quitTime IS NULL
    LIMIT 1`,

    /*
        Leaderboard Loading
        Get TOP10 Player in this service.
    */
    leaderboard: `SELECT 
    player.name, player.kills, player.objectives, player.score
    FROM leaderboard
    INNER JOIN player ON leaderboard.id_player=player.id
    ORDER BY player.score DESC
    LIMIT 10`,
    
    /*
        Leaderboard Trim
        remove old records.
    */
    leaderboard_trim: ``, 

    /*
    */
    registerPlayer: `INSERT INTO handshake.player (\`name\`) VALUES (?);`,
    registerAccount: `INSERT INTO handshake.service_user (\`email\`, \`password\`, \`user_id\`) VALUES (?, ?, LAST_INSERT_ID());`,
    removeAccount: `DELETE FROM handshake.player WHERE id = LAST_INSERT_ID()`,

	login: `SELECT * FROM service_user
    INNER JOIN player ON service_user.user_id=player.id
	WHERE service_user.email = ? AND service_user.password = ?`
}