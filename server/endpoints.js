const apiAddress = 'http://localhost:8080';

module.exports = {
    posts: `${apiAddress}/api/posts`,
    player: `${apiAddress}/api/profile`,
    comments: `${apiAddress}/api/comments`,
    likes: `${apiAddress}/api/likes`,
    leaderboard: `${apiAddress}/api/leaderboard`,
    register: `${apiAddress}/auth/register`,
    login: `${apiAddress}/auth`,
    ping: `${apiAddress}/auth/ping`,
    doComment: `${apiAddress}/api/docomment`,
    doPost: `${apiAddress}/api/dopost`
};