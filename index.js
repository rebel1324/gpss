const express = require("express");
const app = express();
const db = require("./server/database");
const config = require("./server/config");
const bodyParser = require("body-parser");
const socket = require('./socket.js')
//const nextFramework = require('./next.js')

app.use(bodyParser.json())

const apiRoutes = require("./routes/api");
const authRoutes = require("./routes/login");

app.use("/api", apiRoutes)
app.use("/auth", authRoutes)

app.listen(config.port, function () {
    console.log(`Example app listening on port ${config.port}!`);
});